---
layout: markdown_page
title: "Atlassian BitBucket"
---
<!-- This is the template for sections to include and the order to include
them. If a section has no content yet then leave it out. Leave this note in
tact so that others can see where new sections should be added.

### Summary
### Strengths
### Challenges
### Who buys and why
### Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
### Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
### FAQs
 - about the product  <-- comment. delete this line
### Integrations
### Pricing
   - summary, links to tool website  <-- comment. delete this line
### Comparison
   - link to comparison page  <-- comment. delete this line
### Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

### Summary
Atlassian Bitbucket gives teams Git code management, but also one place to plan projects, collaborate on code, test and deploy. It is sold in the SaaS form (Bitbucket.org) and in a self-managed version (Bitbucket Data Center).

### Comments/Anecdotes
* GitLab  employee observation from a recent [HackerNews article](https://news.ycombinator.com/item?id=17908553)
   > Atlassian put the B-team on BitBucket Server and it isn't getting many updates because it is written in a different programming language. This stresses the value of having codebase parity accross products.
* Disparity between BitBucket Server and BitBucket Cloud for a top customer requested feature took Atlassian over 2 years to acknowledge. Customers found out the feature existed in Server but not Cloud once moving tp Cloud. Still not resolved. - [https://bitbucket.org/site/master/issues/12833/branching-models-for-bb-cloud#comment-45982415](https://bitbucket.org/site/master/issues/12833/branching-models-for-bb-cloud#comment-45982415)

### Resources
* [Atlassian BitBucket Website](https://bitbucket.org/product)

### Pricing
- [Bitbucket.org](https://bitbucket.org/product/pricing)
  * Free tier - $0 - Unlimited private repos, Jira Software integration, Projects Pipelines (50 build mins/month), 1GB/month limit on file storage
  * Standard tier - $2/user/month (min $10/month) - Same as Free + 500 build mins/month + 5GB file storage/month
  * Premium tier - $5/usr/month (min $25/month) - Standard + some advanced features + 1000 build mins/month + 10GB file storage/month

- [BitBucket Server Enterprise](https://bitbucket.org/product/enterprise)
  * Server - starting $2k perpetual (25 users, ppu drops roughly every 2x previous tier), includes year maintenance, single server, unlimited priv+pub repos
  * Data Center - $1800/yr (25 users , ppu drops roughly every 2x previous tier) includes annual maintenance, Server + HA, DR, mirroring, SAML 2.0
  * Must buy Data Center if over 2k users.

### Comparison
- [Compare BitBucket Data Center features to GitLab](https://about.gitlab.com/comparison/bitbucket-data-center-vs-gitlab.html)
- [Compare BitBucket.org features to GitLab](https://about.gitlab.com/comparison/bitbucket-org-vs-gitlab.html)