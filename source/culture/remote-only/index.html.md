---
layout: markdown_page
title: "All Remote"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Remote Manifesto

1. Work from anywhere you want
1. Communicate Asynchronously
1. Recognize that the future is unknown
1. Have face-to-face meetings online
1. Daily stand-up meetings are for bonding, blockers, and the future
1. Bond in real life
1. Give credit where it's due and remember to say thank you

For more information on working remotely at GitLab check out our [Remote Manifesto blog post](/2015/04/08/the-remote-manifesto/).

## What Have We Learned about Remote Working?

How do we manage our entire [team](/team/) remotely? Sid Sijbrandij, CEO, shares the [secrets to managing 200 employees in 200 locations](https://www.youtube.com/watch?v=e56PbkJdmZ8).

Our policy of remote work comes from our [value](/handbook/values/) of boring solutions and was a natural evolution of team members choosing to work from home. Remote work allowed for the development of our publicly viewable [handbook](/handbook/). We like efficiency and do not like having to explain things twice.

In on-site companies they take processes, camaraderie, and culture for granted and have it develop organically. In an all-remote company you have to organize it, this is hard to do but as you scale it becomes more efficient while the the on-site organic approach fizzles out.

We have had success bonding with our coworkers in real life through our [Summits](/culture/summits/) that are organized every 9 months and our [Visiting Grants](/handbook/incentives/#sts=Visiting grant).

We have a daily [team call](/handbook/communication/#team-call) to talk about what we did outside of work. There is also a daily [functional group update](/handbook/people-operations/functional-group-updates/) in which one department shares their progress and challenges.

Our [expense policy](/handbook/spending-company-money/) provides for equipment, internet service, office space if they want that, and a local holiday party.

We encourage people to have [virtual coffee breaks](https://work.qz.com/1147877/remote-work-why-we-put-virtual-coffee-breaks-in-our-company-handbook/) and do a [monthly AMA with the CEO](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/2649/diffs).

We have an extensive [onboarding template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md) for new hires and organize a [GitLab 101](/culture/gitlab-101/) to ask questions.




## How Remote Work is Changing the Workforce

1. Documentation of knowledge
1. Fewer Meetings, if you miss one, they are recorded
1. Everything is public, everyone can contribute
1. More flexibility in daily life
1. Reduce interruptions to [productivity](https://www.inc.com/brian-de-haaff/3-ways-remote-workers-outperform-office-workers.html)
1. Cost savings on office space and compensation; Reduce inequality due to bringing better paying jobs to lower cost regions
1. Encourages focus on results, not hours worked
1. Reduce environmental impact due to less commuting

## Tips for Working Remotely

Arguably the biggest advantage of working remotely and asynchronously is the
flexibility it provides. This makes it easy to **combine** work with your
personal life although it might be difficult to find the right **balance**.
This can be mitigated by either explicitly planning your time off or plan when
you do work. When you don't work it is recommended to make yourself unavailable
by turning off Slack and closing down your email client. Coworkers should
allow this to work by abiding by the [communication guidelines](/2016/03/23/remote-communication#asynchronous-communication-so-everyone-can-focus).

If you worked at an office before, now you lack a default group to go out to
lunch with. To look at it from a different perspective, now you can select who
you lunch with and who you do not lunch with. Haven't spoken to a good friend in
a while? Now you can have lunch together.

### Coffee Break Calls

Understanding that working remotely leads to mostly work-related conversations
with fellow GitLabbers, everyone is encouraged to dedicate **a few hours a week**
to having social calls with any teammate - get to know who you work with,
talk about everyday things and share a virtual cuppa' coffee. We want you to make
friends and build relationships with the people you work with to create a more comfortable,
well-rounded environment. The Coffee Break calls are different from the
[Random Room](/handbook/communication/#random-room) video chat, they are meant to give you the option
to have 1x1 calls with specific teammates who you wish to speak with and is not a
random, open-for-all channel but a conversation between two teammates.

You can join the #donut_be_strangers Slack channel to be paired with a random team member for a coffee break. The "Donut" bot will automatically send a message to two people in the channel every other Monday. Please schedule a chat together, and Donut will follow up for feedback.

Of course, you can also directly reach out to your fellow GitLabbers to schedule a coffee break in the #donut_be_strangers Slack channel or via direct message.

### Tips on Ergonomic Working

The goal of [office ergonomics](http://ergo-plus.com/office-ergonomics-10-tips-to-help-you-avoid-fatigue/) is to design your office work station so that it fits you and allows for a comfortable working environment for maximum productivity and efficiency. Since we all work from home offices, GitLab wants to ensure that each team member has the [supplies](/handbook/spending-company-money/) and knowledge to create an ergonomic home office.

Below are some tips from the [Mayo Clinic](http://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/office-ergonomics/art-20046169) how on how arrange your work station. If however, you develop any pains which you think might be related to your working position, please visit a doctor.

- **Chair**: Choose a chair that supports your spinal curves. Adjust the height of your chair so that your feet rest flat on the floor or on a footrest and your thighs are parallel to the floor. Adjust armrests so your arms gently rest on them with your shoulders relaxed.
- **Keyboard and mouse:** Place your mouse within easy reach and on the same surface as your keyboard. While typing or using your mouse, keep your wrists straight, your upper arms close to your body, and your hands at or slightly below the level of your elbows. Use keyboard shortcuts to reduce extended mouse use. If possible, adjust the sensitivity of the mouse so you can use a light touch to operate it. Alternate the hand you use to operate the mouse by moving the mouse to the other side of your keyboard. Keep regularly used objects close to your body to minimize reaching. Stand up to reach anything that can't be comfortably reached while sitting.
- **Telephone**: If you frequently talk on the phone and type or write at the same time, place your phone on speaker or use a headset rather than cradling the phone between your head and neck.
- **Footrest**: If your chair is too high for you to rest your feet flat on the floor — or the height of your desk requires you to raise the height of your chair — use a footrest. If a footrest is not available, try using a small stool or a stack of sturdy books instead.
- **Desk**: Under the desk, make sure there's clearance for your knees, thighs and feet. If the desk is too low and can't be adjusted, place sturdy boards or blocks under the desk legs. If the desk is too high and can't be adjusted, raise your chair. Use a footrest to support your feet as needed. If your desk has a hard edge, pad the edge or use a wrist rest. Don't store items under your desk. GitLab recommends having an adjustable standing desk to avoid any issues.
- **Monitor**: Place the monitor directly in front of you, about an arm's length away. The top of the screen should be at or slightly below eye level. The monitor should be directly behind your keyboard. If you wear bifocals, lower the monitor an additional 1 to 2 inches for more comfortable viewing. Place your monitor so that the brightest light source is to the side.

### Health and Fitness

It is sometimes hard to stay active when you work from your home. GitLab wants to ensure each team member takes care of themselves and dedicates time to stay healthy. Here are some tips that can help to stay healthy and active. You can also join the Slack channel `fitlab` to discuss your tips, challenges, results, etc. with other team members.

1. Try to step away from your computer and stretch your body every hour.
1. To avoid "Digital Eye Strain" follow the [20-20-20 Rule](https://www.healthline.com/health/eye-health/20-20-20-rule#definition). Every 20 minutes look into the distance (at least 20 feet/6 meters) for 20 seconds.
1. There are apps that will remind you to take a break or help you with your computer posture:
 - [PostureMinder](http://www.postureminder.co.uk/)(Windows)
 - [Time Out](https://itunes.apple.com/us/app/time-out-break-reminders/id402592703?mt=12)(macOS)
 - [Awareness](http://iamfutureproof.com/tools/awareness/)(macOS)
 - [SafeEyes](http://slgobinath.github.io/SafeEyes/)(GNU/Linux)
1. Move every day
  - Even when it can be hard to force yourself to move when working the whole day from your home try to go for a walk or do a short excersise for at least 15 minutes / day.
  - There are multiple activities that can be done within a short amount of time like rope jumping, lifting kettlebells, push-ups or sit-ups. It might also help to split the activity into multiple shorter sessions. You can use an app that helps you with the workout, e.g., [7 minute workout](https://7minuteworkout.jnj.com/).
1. Try to create a repeatable schedule that is consistent and try to make a habit out of it. Make sure you enjoy the activity.
1. It can also help to create a goal or challenge for yourself, e.g., registering for a race can force you to excercise.
1. Eat less refined sugar and simple carbs, eat complex carbs instead. Try to eat more vegetables. Don't go to the kitchen to eat something every 15 minutes (it’s a trap!). Keep junk food out of your house.
1. Have a water bottle with you at your desk. You will be more inclined to drink if it's available at all times.
1. Sleep well and take a nap during the day if you need one.
