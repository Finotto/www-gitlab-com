---
layout: markdown_page
title: Licensing and subscription FAQ
---

### How do I purchase a license?

You can purchase a license for GitLab Enterprise Edition (self-hosted) or for 
GitLab.com (hosted by GitLab) on our [Customer Portal](https://customers.gitlab.com/) 
with a credit card.

### How do I renew my license?

1. Log into your account on customers.gitlab.com.
1. Click on Subscriptions.
1. Click on Renew.
1. Enter in the quantity of users to renew.
1. Enter in Users over license.
1. Click on Proceed to checkout.
1. Review Subscription Renew Detail.
1. Click on Confirm Renew.
1. A payment receipt will be e-mailed, you can also access this information under Payment History.

### What does "users over license" mean?

If you've added more users to your GitLab EE instance during the past period 
than you were licensed for, we need to add these additional users to your 
subscription. 

Without adding these users during the renewal process, your license key will 
not work.

You can find the number of *users over license* by going to the `/admin` 
section of your GitLab instance (e.g. `https://example.gitlab.com/admin`). This 
can also be found by clicking the *admin wrench* in the navbar of your instance 
when logged in as an admin.

In the top right section of the admin dashboard, you should see the number to 
enter when asked this during the renewal process.

### Who gets counted in the subscription?

Every person with a GitLab account on the instance is a user and is counted 
in the subscription. Only _active_ users count towards total user count. 
Blocked users do not count, nor do Guest users in an Ultimate license.

For more detail, administrators can find user counts under the 
**Admin wrench** > **Users**. The Active Users tab indicates the users currently 
counted.

### Can I get a trial?

It is possible to obtain a free evaluation license of our enterprise edition for
a 30 day period for up to 100 users. Please visit our 
[free trial page](https://about.gitlab.com/free-trial/) to sign up.

When you decide to purchase a subscription, you will be issued a new license 
key. Should you not take out a subscription, your key will expire at the end 
of your evaluation period.

### Can I use my paid seats on a new set of users?

The seats for your license are generic and are not specific to a user. GitLab does
not use a named license model. 

The seats you buy can be distributed however you choose. If a user leaves your 
organization, you can remove or block that user to free the seat. This seat can 
then be used by another user.

If the additional user is added before the previous seat is blocked, note that 
this may result in a user over license if your maximum users has been reached.

### Can I add more users to my subscription?

Yes. You can add users to your subscription any time during the subscription period. 

You can log in to your account via the
[GitLab Customer Portal](https://customers.gitlab.com) and add more seats or via our 
[Contact Us](https://about.gitlab.com/contact/) page. 

In either case,
the cost will be prorated from the date of quote/purchase through the end end of 
the subscription period. You may also add users and pay for the users over 
license at renewal.

If you're using GitLab.com:
* Simply add more users to the GitLab.com group you've assigned your plan to. 
New users added to your group's plan will be automatically charged a pro-rated amount.

If you've subscribed to a self-hosted plan:
1. Access your account on customers.gitlab.com.
1. Click on Subscriptions.
1. Click on Add more seats.
1. Enter in the total amount of desired users (E.g. You currently have 10 users and want to add 10 more users, enter 20).
1. Proceed to checkout.
1. Review the Subscription Upgrade Detail, this will show a pro-rated charge from the purchase date through the end of your subscription term.
1. Confirm Upgrade
1. A payment receipt will be e-mailed, you can also access this information under Payment History.

### Do I need an additional license if I run more than one server (e.g., for backup, high availability, failover, testing, and staging)?

No, if your GitLab servers cover the same users, you can use
the same license file for all of them.

### What happens when my subscription is about to expire or has expired?

- Starting 30 days before the subscription end date, GitLab will display a
  notice to all administrators informing them of the impending expiration.
- On the day the subscription expires, nothing changes.
- 14 days after the subscription has ended, GitLab will display a notice to all
  users informing them of the expiration, and pushing code and creation of
  issues and merge requests will be disabled.

### What happens if I decide to not renew my subscription?

14 days after the end of your subscription, your key will no longer work and
GitLab Enterprise Edition will not be functional anymore. You will be able to
downgrade to GitLab Community Edition, which is free to use.

### What's the difference between a Group plan and a Personal plan?

On GitLab.com, you can purchase a plan either for your personal account or for a group.
* Group Plans (recommended) apply to all projects stored under a particular group on GitLab.com
* Personal Plans applies to all personal projects that are stored under your username.
 
### How do I purchase a Group Plan on GitLab.com?

You can use GitLab's Customer Portal at https://customers.gitlab.com. Note that 
you must create the group before purchasing the plan.

1. Create a group on GitLab.com
1. Add users to your group
1. Purchase a GitLab.com subscription via https://customers.gitlab.com
1. Select the group from the drop down menu
1. Proceed to checkout

### How do I downgrade my subscription?

Please submit your request to support@gitlab.com.

### Do you support resellers?

We don't currently support reseller purchasing via the portal. If you are a 
reseller looking to purchase GitLab on behalf of your client, please get in 
touch with us using the [Contact Us sales form](https://about.gitlab.com/sales/). 
If you include your billing contact name and email, your physical billing 
address, and the end customer's name, email address and shipping address, we 
will send you (not your customer) a resellers quote which you can execute 
either with a credit card or an EFT. 

You can find details on our reseller program at https://about.gitlab.com/resellers/program.
