---
layout: markdown_page
title: "Marketing"
description: "GitLab Marketing Handbook: Social Media, Corporate Marketing, Lead Generation, BDR, Community Marketing and Product Marketing"
twitter_image: '/images/tweets/handbook-marketing.png'
---

## Welcome to the GitLab Marketing Handbook
{: .no_toc}

The GitLab Marketing team includes four functional groups: Marketing & Sales Development, Community Marketing, Corporate Marketing, and Product Marketing.
{: .note}

----

## On this page
{: .no_toc}

- TOC
{:toc .toc-list-icons}

----

## <i class="fas fa-book fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Handbooks
{: #marketing-handbooks}

- [Blog]
- [Website]
- [Social Marketing]
  - [Social Media Guidelines]
- [Marketing & Sales Development]
   - [Sales Development]
      - [Inbound BDR]
      - [Outbound SDR]
   - [Content Marketing]
   - [Field Marketing]
   - [Marketing Operations]
   - [Online Marketing]
- [Corporate Marketing]
  - [Design]   
  - [Swag]   
  - [Events]   
- [Community Marketing]
   - [Community Advocacy]
   - [Code Contributor Program]
- [Product Marketing]
   - [Partner Marketing]
- [Business Operations]
- [Marketing Career Development]

----

## <i class="fab fa-gitlab fa-fw icon-color font-awesome" aria-hidden="true"></i> GitLab Marketing Purpose

<br />

<div class="alert alert-purple center"><h3 class="purple"><strong>We <i class="fas fa-heart orange font-awesome" aria-hidden="true"></i> GitLab</strong></h3></div>

We think GitLab (.com, CE, and EE) is the fastest way for developers, designers, and IT practitioners to turn their ideas into production software. The GitLab Marketing team is here to do the following:

- Meet board approved company goals and metrics.
- Support and celebrate the GitLab community.
- Enable anyone to contribute to our open source products.
- Evangelize the GitLab product in an authentic and helpful way.
- Help each other to achieve our individual and company OKRs.

## Integrated Campaigns:

The marketing department's effort is organized in Integrated Campaigns. An Integrated Campaign is a communication effort that includes several campaign tactics such as blog posts, emails, events, advertisements, content on about.gitlab.com, videos, case studies, whitepapers, surveys, social outreach, and webinars. An Integrated Campaign will have a campaign theme that summarizes the message we are communicating to our market.    

### Active Campaigns

|name | objective | start date | end date |
|-|-|-|-|
| DevOps | Reposition GitLab from just version control to a single application for the entire DevOps lifecycle | 2018-05-18 | 2018-10-15 |
| Continuous Integration | Expand market awareness of GitLab's integrated CI capability | 2017-12-07  | 2018-12-31 |
| Ultimate Early Adopter | Increase number of GitLab Ultimate customers and gather customer feedback | 2018-05-23 | 2018-09-15 |
| Google Partnership | Reduce customer friction on using GitLab's integration with k8s | 2018-04-05 | 2018-10-05 |
| Free for Edu & OSS | Improve GitLab's brand by affirming our commitment to education and OSS | 2018-05-23 | 2018-09-30 |
| Cloud | Build awareness and educate the market on GitLab's cloud capabilities | 2018-07-01 | 2019-03-31 |
| Security | Build awareness and educate the market on GitLab's security capabilities | 2018-07-01 | 2019-03-31 |


We track Integrated Campaign results in salesforce.com, and costs associated with Integrated Campaigns in Netsuite. In salesforce.com, Integrated Campaigns are tracked as `parent campaigns` and campaign tactics are tracked as `campaigns`. A campaign tactic will have a `Type` and `Type Detail` to identify the category of effort for the purpose of identifying what types of marketing investment are most efficient in terms of the ratio between customer acquisition cost (CAC) and customer lifetime value (LTV).

Different marketing teams handle different aspects of Integrated Campaigns. Which team are you looking for? Here's what each does.

## <i class="fas fa-users fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Team Functional Groups
{: #groups}


### Marketing and Sales Development

Marketing & Sales Development is a confederation of marketing and sales professionals united towards a common purpose: to grow global demand for GitLab. They approach their work with deep empathy for GitLab users, customers, and prospects, and data-driven programs to deliver useful communications.

#### Senior Director, Marketing and Sales Development
{: .no_toc}

[Position Description](/job-families/marketing/director-of-marketing-and-sales-development/){:.btn .btn-purple-inv}
[Handbook][Marketing & Sales Development]{:.btn .btn-purple}

#### Sales Development
{: .no_toc}

Business Development Representatives (BDRs) focus on serving the needs of prospective customers' during the beginning of their buying process. When prospective customers have questions about GitLab, the BDRs answer them when able, or direct them to a technical team member as needed. If upon initial exploration the prospective customer is interested in continuing their exploration of GitLab, BDRs will connect them to an account executive (AE) or strategic account leader (SAL). Sales Development Representatives (SDRs) contact people who work at large organizations to uncover or create early stage sales opportunities for GitLab SALs. Account researchers arm the SDR team with insights about the accounts they are prospecting into including contact discovery, understanding enterprise-wide initiatives that GitLab could assist with, and ensuring accurate data quality of accounts and contact in salesforce.com.

[Position Description](/job-families/marketing/business-development-representative/){:.btn .btn-purple-inv .extra-space}
[Handbook][Sales Development]{:.btn .btn-purple .extra-space}

#### Field Marketing
{: .no_toc}

Field marketers focus on understanding the specific needs of the geographic regions where GitLab operates. They manage marketing activities, such as events and sponsorships, tailored to the needs of the region the activity takes place in.

[Position Description](/job-families/marketing/field-marketing-manager){:.btn .btn-purple-inv}
[Handbook][Field Marketing]{:.btn .btn-purple}

#### Marketing Operations
{: .no_toc}

Marketing operations focuses on enabling the GitLab marketing organization with marketing technology, process, and insights. They are responsible for evaluating, deploying, and administering marketing systems, documenting and improving administrative processes, and analyzing our marketing data to ensure marketers are held accountable to continuously improving their work.

[Position Description](/job-families/marketing/marketing-operations-manager/){:.btn .btn-purple-inv}
[Handbook][Marketing Operations]{:.btn .btn-purple}

#### Online Marketing
{: .no_toc}

Online marketing focuses on managing online advertising, improving the marketing site, website experiments, and search engine optimization (SEO). Online advertising is aimed at increasing the volume of relevant traffic to GitLab's marketing site, website experiments are focused on improving web traffic-to-form submission conversion, and SEO is aimed at ensuring our marketing site ranks for the search engine keywords our audiences care about.

[Position Description](/job-families/marketing/online-growth-manager/){:.btn .btn-purple-inv}
[Handbook][Online Marketing]{:.btn .btn-purple}

#### Content Marketing
{: .no_toc}

Content marketers focus on understanding our audience of developers, IT ops practitioners, and IT leadership. They create useful content for GitLab's audiences, and ensure that the content is delivered to the right audience, at the right time, and in the right way.

[Position Description](/job-families/marketing/content-editor/){:.btn .btn-purple-inv}
[Handbook][Content Marketing]{:.btn .btn-purple}

### Corporate Marketing

Corporate Marketing is responsible for the stewardship of the GitLab brand and the company messaging/positioning. The team is the owner of about.gitlab.com and oversees the website strategy. Corporate Marketing creates global marketing materials and communications and supports the field marketing teams so they can execute regionally while staying true to the GitLab brand. 

[Handbook][Corporate Marketing]{:.btn .btn-purple}

#### Director, Corporate Marketing
{: .no_toc}

The Director, Corporate Marketing is responsbile for the GitLab brand, creative, company messaging/positioning, the [Marketing website](/), PR, customer reference program, and corporate events.

[Position Description](/job-families/marketing/director-corporate-marketing/){:.btn .btn-purple-inv}

#### Corporate Events
{: .no_toc}

Corporate Events oversee strategy and execution for large ”national” events.

[Position Description](/job-families/marketing/corporate-events-manager/){:.btn .btn-purple-inv}

#### Design
{: .no_toc}

Design focuses on creating the visual brand for GitLab. Design supports product marketing, content marketing, lead generation, and almost every function in the marketing team to ensure consistent and clear visual brand. Design is responsible for the [GitLab website](/), swag, event design, content design, and branding.

[Position Description](/job-families/marketing/designer/){:.btn .btn-purple-inv}
[Handbook][Design]{:.btn .btn-purple}

#### Marketing Web Developer/Designer
{: .no_toc}

[Position Description](/job-families/marketing/marketing-web-developer-designer/){:.btn .btn-purple-inv}

### Community Marketing

Community Marketing includes community advocacy and code contributor program functions. The team is focused on answering the following questions:

- What are scalable developer education tools?
- How do we turn in person feedback at events into actionable product requests?
- What are the best and most engaging talks we can give to help educate developers?
- How do we support the community?
- How do we make the documentation even better?
- How do we make it even more fun and easy to get started?
- What is engaging developer content for blog, video, social media?
- How do we build our global meetup plan so word of mouth + make it easy to love GitLab?
- What is the developer GitLab experience?
- How do we use social media to support the community?
- How do field marketing and developer relations work together to support the community?

#### Director, Community Marketing
{: .no_toc}

The Director, Community Marketing develops the strategy, builds and leads the organization responsible for Community Advocacy and Code Contributor Program. They focus on evangelism and advocacy within both the GitLab community and the developer community at large.

#### Developer Program Manager
{: .no_toc}

Developer Program Managers create and manage engagement programs for the various communities to gather feedback, engage in dialogue, educate and evangelize. They develop tools and content to support developers onboarding and adoption of GitLab. The team also sponsors and participates in events and meet-ups, host webinars and give talks. They also capture developer feedback and facilitate 2-way interaction for information and feedback exchange between the Community and GitLab.

[Position Description](/job-families/engineering/developer-advocate){:.btn .btn-purple-inv}
[Handbook][Developer Advocacy]{:.btn .btn-purple}

#### Contributor Program Manager
{: .no_toc}

Contributor Program Managers engage with the GitLab Community by responding to all questions about GitLab asked online. They identify and respond to conversations about GitLab warranting response, they create, optimize and document processes for Community engagement and they continually work to make it easier for the Community to engage with GitLab.

[Position Description coming soon](#){:.btn .btn-purple-inv}
[Handbook][Community Advocacy]{:.btn .btn-purple}

#### Student Program Manager
{: .no_toc}

Student Program Managers develop outreach programs to engage educational institutions to encourage and facilitate the usage of GitLab. They work to ensure tomorrow’s developers have access to GitLab within their education environments. Student Program Managers also create and implement programs for the institutions to utilize, including providing resources for students, supporting student based meet-ups and developing scholarship programs.

[Position Description coming soon](#){:.btn .btn-purple-inv}

#### Merge Request Coach
{: .no_toc}

[Position Description](/job-families/merge-request-coach){:.btn .btn-purple-inv}

#### Community Advocate
{: .no_toc}

[Position Description](/job-families/marketing/community-advocate){:.btn .btn-purple-inv}

### Product Marketing

[Handbook][Product Marketing]{:.btn .btn-purple}

#### Director, Product Marketing
{: .no_toc}

[Position Description](/job-families/marketing/director-product-marketing){:.btn .btn-purple-inv}

#### Product Marketing Manager
{: .no_toc}

[Position Description](/job-families/marketing/product-marketing-manager){:.btn .btn-purple-inv}

#### Technical Product Marketing Manager
{: .no_toc}

Technical Prodcut Marketing is responsbile for technical content and sales tools (demos) development, technical product sales enablement, as well as technical competitive and market intelligence.

[Position Description](/job-families/marketing/technical-product-marketing-manager){:.btn .btn-purple-inv}

#### Analyst Relations Manager
{: .no_toc}

Analyst Relations manages and cultivates relationships with analyst firms. They develop and execute analyst briefing strategies to highlight product, customer and business strategy, progress and success. They regularly hold inquiry sessions with target analysts to gather insights and feedback on strategy, tactics, market dynamics, SWOT, etc. They are also responsbile for defining and executing an analyst briefing strategy to ensure the relevant analysts are kept up to date on company strategy and progress, product strategy and launches and highlighting our customers and community.

[Position Description coming soon](#){:.btn .btn-purple-inv}

#### Partner Marketing Manager
{: .no_toc}

Partner Marketing is responsbile for joint GTM strategy for partnerships. They create solution/product positioning and messaging, business value messaging, and oversee content creation/adaptation, sales and channel enablement and reseller program marketing.


[Position Description coming soon](#){:.btn .btn-purple-inv}
[Handbook][Partner Marketing]{:.btn .btn-purple}

## <i class="fas fa-suitcase fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Production
{: #marketing-products}

<!-- The following HTML blocks are the Marketing Products boxes -->
<!-- DON'T EDIT THIS PART BELOW UNLESS YOU KNOW WHAT YOU'RE DOING :) -->
<div class="row mkt-row">
  <a href="/blog/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/blog.png" alt="GitLab Marketing - Demand Generation - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">BLOG</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="https://gitlab.myshopify.com/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/swag_shop.png" alt="GitLab Marketing - Design - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">SWAG</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="/blog/categories/events/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/location.png" alt="GitLab Marketing - Developer Relations - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">EVENTS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="/handbook/marketing/developer-relations/developer-advocacy/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/news.png" alt="GitLab Marketing - Product Marketing - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">TALKS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
</div>
<!-- NEXT ROLL OF 4 -->
<div class="row mkt-row">
  <a href="https://docs.gitlab.com/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/documentation.png" alt="GitLab Marketing - Demand Generation - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">DOCS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/website.png" alt="GitLab Marketing - Design - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">WEBSITE</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="https://www.youtube.com/playlist?list=PLFGfElNsQthZnwMUFi6rqkyUZkI00OxIV">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/team_alt.png" alt="GitLab Marketing - Developer Relations - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">WEBCASTS</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
  <a href="https://docs.gitlab.com/ee/university/">
    <div class="col-sm-6 col-md-3 mkt-box">
      <div class="thumbnail product">
        <img src="/images/icons/training.png" alt="GitLab Marketing - Product Marketing - Icon">
        <div class="caption">
          <p class="center description">
            <h4 class="orange">UNIVERSITY</h4>
          </p>
        </div>
      </div>
    </div>
  </a>
</div>
<!-- END OF MARKETING PRODUCTS -->


## <i class="fas fa-cogs fa-fw icon-color font-awesome" aria-hidden="true"></i> Meetings and structure
{: #meetings}

These are just the required meetings for team members and managers. Of course, meetings are encouraged when it expedites a project or problem solving among members, so the team and company. Don't be afraid to say "Hey, can we hangout?" if you need help with something.

### Weekly 1:1 (New hire: First month - managers with all direct reports)
{: .no_toc}

**Meeting goal: For manager to help new team member onboard quickly.**

**Run time: 30 minutes**

All managers should have a weekly 1:1 with their direct reports in the first month of employment, starting with the first day of employment where possible.

The first meeting should run as follows:

- Welcome and make sure new hire is working through onboarding tasks.
- Suggest people on the marketing team and beyond to meet with.
- Manager should make sure all technology needs are taken care of.
- Manager should answer or help to find proper resource for any questions.
- Manager should create a Google Doc private 1:1 agenda for all recurring 1:1's and add to the description of the calendar invite.

The agenda of the following 1:1s should be the same as the recurring Bi-weekly 1:1s with time set aside to answer any questions about onboarding.

From the second month on, all managers should have twice monthly
[(bi-weekly) meetings](#bi-weekly-11-all-managers-with-all-direct-reports) with all
of his or her direct reports.

### Weekly 1:1 (All Directors with CMO)
{: .no_toc}

**Meeting goal: For CMO to provide direction, help prioritize and receive feedback from all Directors.**

**Run time: 30 minutes**

All directors should have weekly meetings with the CMO.

The meeting should run as follows:

- Always add agenda items to Google Doc agenda in description of meeting invite.
- First discuss any issues or problems that the CMO can help with. Any roadblocks or disagreements?
- Go through the agenda and answer any questions.
- Review status of current quarter OKRs.
- Action items should be highlighted with owner assigned in the doc.
- All action items should be handled before next 1:1 unless otherwise noted with due date.
- Discuss upcoming OKRs if close to end of a quarter.

### Bi-weekly 1:1 (Managers with all direct reports)
{: .no_toc}

**Meeting goal: For manager to remove roadblocks and help prioritize so team member can be effective.**

**Run time: 30 minutes**

All managers should have twice monthly (bi-weekly) meetings with all of his or her direct reports.

The meeting should run as follows:

- Always add agenda items to Google Doc agenda in description of meeting invite.
- First discuss any issues or problems that the manager can help with. Any roadblocks or disagreements?
- Go through the agenda and answer any questions.
- Review status of current quarter OKRs.
- Action items should be highlighted with owner assigned in the doc.
- All action items should be handled before next 1:1 unless otherwise noted.

### Bi-weekly Marketing Team Call (All Marketing team members)
{: .no_toc}

**Meeting goal: Provide visibility and alignment across the Marketing team and provide forum for cross-functional discussion and Q&A.**

**Run time: 60 minutes**

The Marketing team meets bi-weekly to review announcements, strategy developments, company updates; per functional area, review metrics, prior period accomplishments and blockers, review upcoming key activities; and have a forum for roundtable discussion and Q&A.

The meeting should run as follows:

- Prior to the meeting, each functional area lead updates their content in the [Marketing Team Update](https://docs.google.com/presentation/d/112N-ICgqXn911EebrVhRj-pd5Z_coWi3TSS3VJwV0to/edit#slide=id.g1c73db867d_0_0) doc.
- The CMO leads off the meeting with announcements, and strategy, company and org updates.
- Each functional lead reviews prior accomplishments and upcoming plans. The Lead will also share relevant team pdates and share cross-functional asks
- The remaining time is used for open discussion and Q&A.
- Interuption is encouraged! Team members are encouraged to interupt and ask questions throughout.
- All team members are also encouraged to bring forward discussion topics that they want to share with the team or to have covered during the call.
- Action items are recorded and owners assigned. Owners are responsible to report back with the outcome of the action item.

### Quarterly 1:1 (All members of Marketing Team with CMO)
{: .no_toc}

**Meeting goal: For CMO to help with any questions and career path discussion.**

**Run time: 30 minutes**

All members of the marketing team who are not direct reports should meet with their executive management (CMO) once every quarter. If questions or concerns arise, please don't hesitate to reach out directly for an impromptu discussion via chat, email or phone call.

The meeting should run as follows:

- First discuss any issues or problems that the CMO can help with. Any roadblocks or disagreements?
- Talk about career development and opportunities for growth.
- Walk through quarterly OKRs to make sure they are relevant and see if help is needed.
- Discuss upcoming OKRs if close to end of a quarter.

### Quarterly Marketing Planning Meeting (Marketing Leadership Team - CMO + Directors)
{: .no_toc}

**Meeting goal: Planning for the upcoming quarter**

**Run time: Two Half Day sessions**

CMO builds agenda and assigns ownership for each section.

The meeting should run as follows:

- CMO leads the review of our marketing strategies from the last quarter.
  - Review OKR results including key metrics
  - What worked? What didn't work? Concerns and blockers? What should we do more of?
- Team agrees on what key takeaways are from the last quarter.
- Team agrees on what Marketing OKRs should be for the upcoming quarter.

## <i class="far fa-thumbs-up fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing team SLAs (Service Level Agreements)
{: #sla}

When working remotely in a fast-paced organization, it is important for a team to agree on a few basic service level agreements on how we would like to work together. With any of these, things can come up that make it not possible to meet the SLAs, but we all agree to use best effort when possible.

- Respond to your emails by end of next business day.
- Respond when you are cc'd with an action item on issues by end of next business day.
- Be on time to meetings. We start at on time.
- Acknowledge receipt of emails (community@, FYIs) by BCC'ing the list.
- Try not to email co-workers on weekends. Try out [Boomerang](http://www.boomeranggmail.com/) and set all your emails to send Monday morning at 6 AM. People will think you're up and working early! Time off is important. We all have stressful weeks so please unplug on the weekends where possible.
- Do not ping someone in a public channel on chat on the weekends. This is rude.

## <i class="far fa-file-code fa-fw icon-color font-awesome" aria-hidden="true"></i> Marketing Handbook Updates
{: #handbook}

Anything that is a process in marketing should be documented in the Marketing Handbook.

- Format of all pages should be as follows:
    - Welcome to the Handbook.
    - Functional group overview if handbook for entire functional group or organization.
    - "On this page" index of all top level headers on the current page ([create a ToC]).
    - Links to other handbooks included on this page.
- Rather than create many nested pages, include everything on one page of your role's handbook with an index at the top.
- Each role should have a handbook page.
- If more than one person are performing a role, the task should be shared to update the handbook with all processes or guidelines.
- Follow the [Markdown Style Guide] for about.GitLab.com.

## <i class="fas fa-rocket fa-fw icon-color font-awesome" aria-hidden="true"></i> How to contact marketing
{: #contact-marketing}

- [**GitLab Marketing public issue tracker**](https://gitlab.com/gitlab-com/marketing/issues/); please use confidential issues for topics that should only be visible to team members at GitLab
- You can also send an email to the Marketing team (see the "Email, Slack, and GitLab Groups and Aliases" google doc for the alias).
- [**Chat channel**](https://gitlab.slack.com/archives/marketing); please use the `#marketing` chat channel for questions that don't seem appropriate to use the issue tracker or the internal email address for.

### Requests from other teams

#### Social ([@GitLab](https://twitter.com/gitlab) only, [@GitLabstatus](https://twitter.com/gitlabstatus) is managed by [Infrastructure](/handbook/engineering/infrastructure/))

Everyone posts their own social updates, to the extent possible. If you want to request that something in one of these categories be posted, reach out to the point person below. They reserve the right to say no to your request, and copy in all of these categories may be adjusted by a marketing team member to ensure consistency in our brand voice.

- Events: Jr. Content Editor
- Release & technical posts/product updates: Technical writer
- User questions/comments on Twitter: Community Advocates
- Leadgen campaigns: Content team
- UX Design: UX Lead
- People Ops: Jr. Content Editor
- Press coverage: Post in #marketing for assistance
- RTs of mentions: Post in #twitter
- CEO statement/posts: Post in #twitter

#### Blog post editing

- Product release posts: Product team
- Technical community posts/tutorials: Incoming technical editor
- CEO statements/updates: CMO
- Culture posts: we have de-prioritized these and are not actively publishing them for now. If you have an idea for a post, please post in #content.
   - Plan on some delay when you pitch, so think about whether your post will still be relevant in 1 month or more.
   - If you want a blog post to be published, you should be prepared to write it and format it independently before expecting a review. An easy way to do this is to copy the latest blog post file and edit it, filling in all the fields with your information and post text. Be sure to add a public domain/creative commons cover image, and [attribute properly](/handbook/marketing/blog/#cover-image) at the bottom of the post.

#### Sponsorship

We are happy to sponsor events and meet-ups where a marketing benefit exists,
subject to approval by Field Marketers. These sponsorships may be in cash or in
kind, depending on individual circumstances.

Organizational or project sponsorships may also be considered where a marketing
benefit exists. Typically, these sponsorships will be in kind - e.g., developer
time commitments, or [subsidized / free GitLab licenses](https://about.gitlab.com/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/).

Cash sponsorship of projects or organizations may be considered only in
exceptional cases - for example, if a project or organization that GitLab
depends on is struggling to survive financially.

#### New Ideas for Marketing Campaigns or Events

- Ideas for marketing campaigns
   - We begin by asking: "Who is this for? What do they think? What do we want them to think?
   - If you have a good idea of who the campaign is for and why it would benefit them and us, create an issue in the marketing repo detailing the campaign concept, and tag the content and product marketing teams.
- Ideas for events
  - We are constantly on the look out for high value events, both large and small, to attend or sponsor, in order to engage with our target audiences.
  - If you have an event that you want to recommend we look into, create an issue in the marketing repo describing the event and rationale to attend/sponsor, and tag the field marketing manager.
- Both teams reserve the right to decline, but we love hearing your ideas! Understand that we need to fit every request into a program we’re running to meet our OKRs, as well as balance the needs of our community and our business.
- If it’s an item you can execute without much help, you’re more likely to be given a green light (e.g., a wallpaper that the design team can easily create).

Note: The marketing team owns content on marketing pages; do not change content or design of these pages without consulting the Online Marketing Manager. Marketing will request help from product/UX when we need it, and work with them to ensure the timeline is reasonable.

### Chat Marketing channels
{: #chat}

We use our chat internally as a communication tool. The Marketing channels are as follows:

- `#Marketing`: General marketing channel. Don't know where to ask a question? Start here.
- `#bdr-team`: For the Inbound BDR team
- `#cfp`: All call for speakers will be posted here.
- `#content`: Questions about blog posts, webcasts, the newsletter or other marketing content? This is the place to ask.
- `#devrel`: A channel for the developer relations team to collaborate.
- `#docs`: Technical writing and documentation questions? This is your room.
- `#events`: Everything you want to know about events.
- `#gitlab-pages`: Where we discuss everything related to GitLab Pages.
- `#marketing-design`: Discuss, feedback, and share ideas on Marketing Design here.
- `#marketo-users` Having issues with Marketo? Ask here first.
- `#social`: Twitter, Facebook, and other social media questions?
- `#swag`: The discount code for GitLabbers ordering on the swag store can be found in the channel details.
- `#twitter`: Use this channel to suggest we tweet about something.
- `#support`: Ask any and all technical questions here for a quick response.
- `#sfdc-users`: Having issues with SFDC? Ask here first.
- `#product-marketing`: Discuss, feedback related to product news, features and vision
- `#website`: Discuss topics related to website redesign project

### Marketing email alias list
{: #email}

- Community@ company domain: external email address for sending confirmation emails related to GitLab products. Replies are forwarded to Zen Desk support    
- Content@ company domain: external email address associated with management of our SlideShare account. Replies are forwarded to Sr. Director of Marketing & Sales Development, Content Marketing Manager & team   
- Events@ company domain: external email address for sending live, VIP &amp; in-person training related emails. Replies go to Field Marketing Manager and Marketing OPS Manager     
- Giveaways@ company domain: external email address for receiving content & social media related promotional giveaways. Replies go to Content Marketing Team, Field Marketing Manager and Marketing OPS Manager    
- Leads@ company domain: external email address for internal Lead alerts. Replies go to Marketing OPS Manager and Online Marketing Manager
- News@ company domain: external email address used to send newsletter. Replies go to Online Marketing Manager
- MarketingOPS@ company domain: external email address used to direct generic operational requests to Marketing OPS Manager and Online Marketing Manager
- MarketingSFDC@ company domain: external email address associated with management of Salesforce. Replies forward to Online Marketing Manager, Field Marketing Manager, Sr. Director of Marketing & Sales Development, Product Marketing Manager and Content Marketing Manager
- SecurityAlerts@ company domain: external email address used to send security alerts. Replies go to Online Marketing Manager   
- Sponsorships@ company domain: external email address used to manage sponsor requests from community. Replies forward to Community Advocate Team
- Support@ company domain: external email address for sending Breaking Change and/or support related customer communications. Replies go to Zen Desk support     
- Surveys@ company domain: external email address for sending the Developer Survey and/or related surveys. Replies go to Content Team, Sr. Director of Marketing & Sales Development and Product Marketing Manager   
- Webcasts@ company domain: external email address for sending webcast related emails. Replies go to Content Marketing Team and Online Marketing Manager

<!-- IDENTIFIERS -->

[cmo]: /job-families/chief-marketing-officer/
[create a ToC]: /handbook/product/technical-writing/markdown-guide/#table-of-contents-toc
[Markdown Style Guide]: /handbook/product/technical-writing/markdown-guide/

<!-- HANDBOOKS -->

[Blog]: /handbook/marketing/blog/
[Website]: /handbook/marketing/website/
[Content Marketing]: /handbook/marketing/marketing-sales-development/content/
[Community Marketing]: /handbook/marketing/community-marketing/
[Community Advocacy]: /handbook/marketing/community-marketing/community-advocacy/
[Code Contributor Program]: /handbook/marketing/community-marketing/code-contributor-program/
[Corporate Marketing]: /handbook/marketing/corporate-marketing
[Design]: /handbook/marketing/corporate-marketing/#design
[Field Marketing]: /handbook/marketing/marketing-sales-development/field-marketing/
[Marketing & Sales Development]: /handbook/marketing/marketing-sales-development/
[Marketing Operations]: /handbook/marketing/marketing-sales-development/marketing-operations/
[Online Marketing]: /handbook/marketing/marketing-sales-development/online-marketing/
[Partner Marketing]: /handbook/marketing/product-marketing/partner-marketing/
[Product Marketing]: /handbook/marketing/product-marketing/
[Business Operations]: /handbook/business-ops/
[Sales Development]: /handbook/marketing/marketing-sales-development/sdr/
[Inbound BDR]: /handbook/marketing/marketing-sales-development/sdr/#how-to-bdr
[Outbound SDR]: /handbook/marketing/marketing-sales-development/sdr/#how-to-sdr
[Social Marketing]: social-marketing/
[Social Media Guidelines]: /handbook/marketing/social-media-guidelines/
[Swag]: /handbook/marketing/corporate-marketing#swag
[Events]: /handbook/marketing/corporate-marketing#events
[Marketing Career Development]: /handbook/marketing/career-development  

<!-- Marketing Team: GitLab.com Handle -->

[cmattrex]: https://gitlab.com/u/cmattrex
[emilykyle]: https://gitlab.com/u/emilykyle
[lindberg]: https://gitlab.com/u/lindberg
[jjcordz]: https://gitlab.com/u/jjcordz
[lukebabb]: https://gitlab.com/u/lukebabb
[evhoffmann]: https://gitlab.com/u/evhoffmann
[rebeccad]: https://gitlab.com/u/rebeccad
[courtlandia]: https://gitlab.com/u/courtlandia
[elsje]: https://gitlab.com/u/elsje
[kyla]: https://gitlab.com/u/kyla
[mollybeth]: https://gitlab.com/u/mollybeth
[Cbrady]: https://gitlab.com/u/Cbrady
[jburton]: https://gitlab.com/u/jburton
[djeffeos]: https://gitlab.com/u/djeffeos
[williamchia]: https://gitlab.com/williamchia
[LaniceSims]: https://gitlab.com/LaniceSims
[hschuler]: https://gitlab.com/hschuler
[bwilliamson]: https://gitlab.com/bwilliamson
[mhamilton]: https://gitlab.com/mhamilton
[Adam.Pestreich]: https://gitlab.com/Adam.Pestreich

<!-- EXTRA STYLES APPLIED FOR THIS PAGE ONLY -->

<style>
.purple {
  color: rgb(107,79,187) !important;
}
.orange {
  color:rgb(252,109,38) !important;
}
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
.btn-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-purple:hover {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv {
  color: #fff;
  background-color: rgb(107,79,187);
  border-color: #403366;
}
.btn-purple-inv:hover {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: #403366;
}
.btn-orange {
  color: rgb(252,109,38);
  background-color: #fff;
  border-color: rgb(226,67,41);
}
.btn-orange:hover {
  color: #fff;
  background-color: rgb(252,109,38);
  border-color: rgb(226,67,41);
}
.product.thumbnail img {
  display: block;
  max-width: 50%;
  margin: 20px auto;
}
.thumbnail img {
  display: block;
  max-width: 30%;
  margin: 20px auto;
}
.caption h4 {
  text-align: center;
}
.mkt-box {
  padding-bottom: 10px;
  padding-top: 10px;
  cursor: pointer;
}
.mkt-box:hover {
  /*border-radius: 5px;*/
  box-shadow:0 1px 5px rgba(0,0,0,0.3), 0 0 2px rgba(0,0,0,0.1) inset;
}
.mkt-row {
  padding-top: 20px;
  padding-bottom: 5px;
}
.mkt-row a:focus {
  outline: none;
}
.modal-header h2 {
  margin-top: 0;
}
.modal-footer p {
  margin-bottom: 0;
}
.center {
  text-align: center;
  display: block;
  margin-right: auto;
  margin-left: auto;
}
.description {
  color: #999;
}
.extra-space {
  margin-bottom: 5px;
}
.alert-purple {
  color: rgb(107,79,187);
  background-color: #fff;
  border-color: rgba(107,79,187,.5);
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
ul.toc-list-icons li ul li i.slack {
  color: rgb(224,23,101);
}
ul.toc-list-icons li ul li i.email {
  color: rgb(192,0,0);
}
</style>
