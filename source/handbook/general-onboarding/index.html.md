---
layout: markdown_page
title: "GitLab Onboarding"
---

All onboarding steps in the [onboarding issue template](https://gitlab.com/gitlab-com/people-ops/employment/blob/master/.gitlab/issue_templates/onboarding.md) which is part of the [People Ops Employment project](https://gitlab.com/gitlab-com/people-ops/employment/issues).

## Other pages

* [Developer onboarding](/handbook/developer-onboarding)
* [UX Designer onboarding](/handbook/uxdesigner-onboarding)
* [UX Researcher onboarding](/handbook/uxresearcher-onboarding)
* [Support onboarding](/handbook/support/onboarding/)
* [Consultant onboarding and offboarding](/handbook/general-onboarding/consultants/)
* [Onboarding Processes](/handbook/general-onboarding/onboarding-processes)
