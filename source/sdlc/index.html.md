---
layout: markdown_page
title: Software Development Lifecycle (SDLC)
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Definition

The modern Software Development Lifecycle (SDLC) consists of multiple phases.
It starts with planning an idea and ends at measuring the metrics of running it
in production.

## Most important organizational process

[Every company is becoming a software company.](https://www.forbes.com/sites/techonomy/2011/11/30/now-every-company-is-a-software-company/)
Therefore, the SDLC is becoming the most important organizational process.
Effective software development is an essential skill to create value, attract
great people, and keep applications secure. To enable this skill, organizations
are adopting SDLC stacks that help this workflow.

## Stacks

There are a couple of organizations that are building a stack for the SDLC.
Below, we've listed the [stages of the DevOps
toolchain](https://en.wikipedia.org/wiki/DevOps_toolchain), [product
categories](/handbook/product/categories/), and the products from the different
vendors.

| Stage     | Product category         | GitLab                    | GitHub             | Atlassian self hosted / SaaS   | Legacy Open Source |
|-----------|--------------------------|---------------------------|--------------------|--------------------------------|--------------------|
| Plan      | Portfolio management     | GitLab Portfolio management | n/a              | JIRA Portfolio                 | n/a                |
| Plan      | Issue tracking           | GitLab Issues             | GitHub Issues      | JIRA / Trello                  | Redmine            |
| Create    | Version control          | GitLab SCM                | GitHub             | BitBucket Server / .org        | SVN                |
| Create    | Code review              | GitLab SCM                | GitHub             | Crucible / BitBucket.org       | Gerrit             |
| Verify    | Continuous integration   | GitLab CI                 | Travis CI          | Bamboo / BitBucket CI          | Jenkins CI         |
| Verify    | Security testing         | GitLab SAST               | Snyk               | n/a                            | SonarQube          |
| Package   | Container registry       | GitLab Container Registry | Docker Trusted Registry | n/a                       | Jfrog Artifactory  |
| Release   | CD/Release automation    | GitLab CD                 | Codefresh          | Bamboo / BitBucket Deployments | Jenkins Pipeline   |
| Configure | Configuration management | GitLab Secret variables   | n/a                | n/a / Environment variables    | Puppet             |
| Monitor   | Monitoring               | GitLab Metrics            | New Relic          | n/a                            | InfluxDB           |

## Interfaces

Below we've listed interfaces that are needed between the different product categories. They are sorted by product category as listed above, with the earlier product category listed first.

1. Issue tracking <=> Kanban boards, preferably they show the same issues.
1. Issue tracking <=> Version control, close issues when you merged code in your branch.
1. Issue tracking <=> Code review, the code review has a link to the issue it is related to.
1. Issue tracking <=> CD/Release automation, see which changes are implemented by which deploy / are live and where.
1. Issue tracking <=> Monitoring, link the initiave to the impact on metrics.
1. Kanban boards <=> Version control, close issues when you merged code in your branch.
1. Version control <=> Code review, the code review happens on a branch that is updated.
1. Version control <=> Continuous integration, run CI automatically on the default branch, see CI status per branch.
1. Version control <=> CD/Release automation, see whether a particular commit is live somewhere.
1. Version control <=> Security testing, see whether a commit is vulnerable / with out of date dependencies.
1. Code review <=> Continuous integration, see the test results in the code review screen.
1. Code review <=> Security testing, see the test results in the code review screen.
1. Code review <=> CD/Release automation, see and control pushing to new environments in the code review screen.
1. Code review <=> Monitoring, see the effect of a code change on the metrics.
1. Continuous integration <=> Security testing, run security testing as part of CI.
1. Continuous integration <=> Container registry, push the container that is built to the registry.
1. Continuous integration <=> CD/Release automation, deploy if green, or don't deploy when red.
1. Continuous integration <=> Configuration management, configure the testing.
1. Security testing <=> CD/Release automation, prevent insecure code from being deployed.
1. Security testing <=> Container registry, scan the container registry.
1. Container registry <=> CD/Release automation, pull the container.
1. CD/Release automation <=> Configuration management, configure the deployment.
1. CD/Release automation <=> Monitoring, see the release in the monitoring.
1. Configuration management <=> Monitoring, configure the monitoring.

## Strategies

1. GitLab covers all the DevOps product categories with the [emergent benefits](/direction/#emergent-benefits-of-a-single-application) of a [single application](/direction/#single-application) and
1. GitHub follows a marketplace strategy where other vendors cover most of the product categories, this doesn't have the advantages of a [single application](/direction/#single-application).
1. Atlassian covers most of the product categories but the user or reseller has to integrate them; this doesn't have the advantages of a [single application](/direction/#single-application).

## Pricing

- [Self-managed](/products/)
- [GitLab.com](/gitlab-com/)
- [Pricing handbook](/handbook/product/pricing/)

## Delivery

| Subject                                    | GitLab         | GitHub                | Atlassian                                       |
|--------------------------------------------|----------------|-----------------------|-------------------------------------------------|
| Preferred platform                         | Kubernetes     | Heroku                | n/a                                             |
| Single tenant install (self-managed)        | Integrated     | Needs other products  | Separate products, CI/CD not actively developed |
| Multi tenant install (SaaS)                | Integrated     | Needs other products  | Includes CI/CD, issues in JIRA, no monitoring   |

## Cloud native workflow

Cloud native means developing applications to run in the cloud.
The platform for deploying these applications is switching from Virtual Machines (AWS) to Container Schedulers (Kubernetes).
Cloud native applications are split up into [micro services](https://martinfowler.com/articles/microservices.html).
This means one application consists of many services that have their individual project and code base.
To handle these cloud native workflows GitLab has [sub-groups](https://docs.gitlab.com/ee/user/group/subgroups/), [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/2517), and [multi-project pipelines](https://gitlab.com/gitlab-org/gitlab-ee/issues/933).

## An integrated product brings emergent benefits

GitLab is the only integrated product for the SDLC; all others are combinations of different products.
Having one product makes for a much better user experience because there is one UI, better security because of consistent permission settings, and less time spent on administration and integration.
Apart from that, this 'development operating system' has some emergent properties that wouldn't be possible otherwise:

- [Auto DevOps](https://gitlab.com/gitlab-org/gitlab-ee/issues/2517) with features such as Auto ChatOps and Auto Review Apps.
- [Cycle Analytics](/features/cycle-analytics/) that allow you to measure and reduce the time from planning to monitoring.
- [ConvDev Index](https://gitlab.com/gitlab-org/gitlab-ce/issues/30469) to help spread best practices.

These emergent properties allow the following emergent benefits:

- Go from idea to value faster and more consistently.
- Creating better business outcomes, requiring fewer people, having more security.
- Transforming the company from agile to [conversational development](http://conversationaldevelopment.com/).

## Open source is the future of software development

GitLab is developed out in the open with most code available under an open source license.
This allowed more than 2,000 experts to contribute their process in the form of code.
GitLab distills the greatest collection of DevOps best practices into a cloud native workflow.
Our integrated product allows you to stand on the shoulders of many experts.

## Related methodologies and software lifecycle concepts

- [Agile](http://agilemanifesto.org/)
- [DevOps](https://en.wikipedia.org/wiki/DevOps)
- [Conversational Development](http://conversationaldevelopment.com/) (ConvDev)
- [Application Lifecycle Management](https://en.wikipedia.org/wiki/Application_lifecycle_management) ALM is defined on wikipedia as "ALM is a broader perspective than the Software Development Lifecycle (SDLC), which is limited to the phases of software development such as requirements, design, coding, testing, configuration, project management, and change management. ALM continues after development until the application is no longer used, and may span many SDLCs."
- [Systems Development Life Cycle](https://en.wikipedia.org/wiki/Systems_development_life_cycle)
- Planning to monitoring is a way to refer to the SDLC cycle of chatting about an idea to measuring it in production.
- [Remote Only](http://www.remoteonly.org/) organizations use SDLC tools to work effectively while they are distributed.
