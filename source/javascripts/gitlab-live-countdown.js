(function() {
  var daysCount = document.getElementById('days-count');
  var hoursCount = document.getElementById('hours-count');
  var minutesCount = document.getElementById('minutes-count');
  var secondsCount = document.getElementById('seconds-count');

  var countdownDate = new Date('Sep 20, 2018 15:00:00' ).getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {
    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countdownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    if (days.toString().length === 1) {
      days = '0'  + days;
    }
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    if (hours.toString().length === 1) {
      hours = '0'  + hours;
    }
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    if (minutes.toString().length === 1) {
      minutes = '0'  + minutes;
    }
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    if (seconds.toString().length === 1) {
      seconds = '0'  + seconds;
    }

    // Output the result in an element with id=' demo'
    daysCount.innerHTML = days;
    hoursCount.innerHTML = hours;
    minutesCount.innerHTML = minutes;
    secondsCount.innerHTML = seconds;

    // If the count down is over, write some text
    if (distance < 0) {
      clearInterval(x);
      // Remove the countdown and add the livestream youtube embed
    }
  }, 1000);
})();
