---
layout: job_page
title: "Director of Sales and Customer Enablement"
---

We have an exciting opportunity for a seasoned Global Sales and Customer Enablement leader to join a growing software DevOps organization. Reporting to the CRO, you will be responsible for building and managing a virtual, world-class global sales training and enablement program.

## Responsibilities

* Develop and lead the successful execution of the sales enablement plan and programs to ensure internal and external customers and partners are equipped with content, resources, tools and training to effectively differentiate and sell the GitLab solution.
* Oversee the development of education and training content in collaboration with marketing and other SMEs to identify and develop core curriculum, customized regionally as needed, for various internal and external audiences.
* Lead and facilitate sales training activities, including new hire training and onboarding, ongoing sales meetings, and regional/sales team training (includes all levels of the sales team, customer success, and BDR/SDR, as well as external customers and channel partners).
* Ensure alignment of sales enablement activities with key internal and external events, such as new product releases, campaigns, and essential industry trade shows.
* Manage and drive online, self-paced on-boarding and result-driven continual learning.
* Leverage Salesforce.com CRM infrastructure and other tools for delivering formal and informal/social learning.
* Develop sales competency assessment programs, as well as partner certification programs.
* Measure and report on the effectiveness of sales enablement investments and the programs conducted. Determine opportunities for improving the sales learning experience, and identify innovative techniques for delivery.


## Requirements

* 10+ years software sales experience with expertise in DevOps and/or Open Source, preferably in sales enablement or management.
* Working knowledge of sales training technology and methodologies; demonstrable experience with sales enablement concepts, practices, and procedures.
* Relevant experience preparing, developing, and executing global sales teams’ effectiveness strategies, tactics and action plans for a technology platform.
* Ability to quickly understand technical concepts and explain them to audiences of varying technical expertise (sales team, customers, channel partners, and internal support partners).
* Experience growing within a small start-up. Strong ability to interact and influence effectively with C-level executives and team members.
* Exceptional written/verbal communication and presentation skills.
* Team player with strong intrapersonal skills, skilled at project management and cross-functional collaboration.
* Ability to thrive in a fast-paced, unpredictable environment.
* You share our values, and work in accordance with those values.
