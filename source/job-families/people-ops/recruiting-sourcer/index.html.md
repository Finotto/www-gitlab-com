---
layout: job_page
title: "Recruiting, Sourcing Specialist"
---

GitLab is looking for a sourcing expert with experience in finding the best candidates for a high-tech, distributed company. How you build relationships and how you subsequently build sourcing strategies to find the best talent in the world is critical. A big picture thinker with experience in an international, highly cross-functional organization who has strong business acumen, will be most successful in the role. Attention to detail and experience with market mapping and deep research is equally as critical. Finally, we’re looking for in-depth understanding and background in hiring amazing people in at least two for these areas: Engineering, Sales, Marketing, and Product.

For the Recruiting Sourcer - EMEA, the role will be based in Europe, The Middle East, or Africa. In this role, it is critical for the candidates to have experience in Sales and Marketing. Being fluent in English is required to be successful at GitLab, but additional language skills are a huge plus.

This role will report to the Recruiting Director.

## Responsibilities

* Source for the best passive candidates in the global market.
* Being willing to look wherever is necessary to find the talent we need, with a focus on our values and requirements.
* Put meaningful focus on building a diverse pipeline.
* Partner directly with in-house recruiters and business leaders globally to understand specific needs to attract and engage with top talent.
* Someone who is highly curious and asks great questions to build their business acumen will be especially successful in the role.
* Strategically utilize LinkedIn, online research, events, etc. to engage highly passive, sought after candidates.
* Develop a strong relationship with candidates and make judicious decisions on fit for a particular role or team, in addition to thinking through fit for our unique culture.
* Map out individual markets and gather intelligence around specific talent pools, using that knowledge to identify top talent.

## Requirements

* Experience sourcing and research at all levels, preferably in a global capacity within the software industry, open source experience is a plus.
* Proven success in sourcing for sales and/or technical positions. Sales is critical for the role in EMEA.
* Demonstrated ability to effectively source passive candidates. This is a fully outbound role.
* Experience with competitive global job markets preferred.
* Previous experience in sourcing in low-cost regions for engineering/development talent would be a plus.
* Focused on delivering an excellent candidate experience.
* Ambitious, efficient, and stable under tight deadlines and competing priorities.
* Remote working experience in a technology startup will be an added advantage.
* Ability to build relationships with managers and colleagues across multiple disciplines and timezones.
* Working knowledge using an candidate tracking systems. Lever is a plus.
* Outstanding written and verbal communication skills across all levels.
* Willingness to learn and use software tools including Git and GitLab.
* College / University degree in Marketing, Human Resources, or related field from an accredited institution preferred.
* Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Specialties

Read more about what a [specialty](/handbook/hiring/#definitions) is at GitLab here.

### Diversity Sourcing Specialist

The Diversity Sourcing Specialist will be responsible for the execution of GitLab’s diversity talent sourcing and outreach strategy in support of GitLab’s hiring and candidate experience goals. Success will be measured through the development of quality community networks, candidate relationships and ultimately an increase of highly qualified candidates globally. In partnership with recruiting leadership and other stakeholders this role will develop sources for many lines of business across technical and non-technical roles to increase diverse prospects in GitLab’s talent pipeline.

The ideal candidate will have passion and experience around diversity talent practices, innovative recruiting strategies and technology, community outreach and events, labor market research, competitive market intelligence, and developing and nurturing relationships with talent.

* Responsibilities
  * Engage and activate diverse talent through relationships and networking with universities, professional organizations, community organizations, diversity forums and conferences, and other recruitment channels.
  * Identify, prescreen, and evaluate both passive and active candidates and consistently generate a healthy pipeline of high quality diverse candidates thru creative sourcing techniques.
  * Understand unique role and business requirements and qualify and match candidates with opportunities inside GitLab.
  * Develop business-specific diversity sourcing plans and strategies.
  * Create and execute narratives, branding and social media campaigns to support external messaging and engagement in partnership with internal teams
  * Partner internally with GitLab’s Diversity and Belonging team and ERGs to coordinate sourcing and recruiting activities at Diversity outreach and engagement events;
  * Manage candidate tracking mechanisms and data systems (e.g. CRM) and relationship management strategies. Conduct data analysis and reporting
* Requirements
  * Relationship building: outgoing, builds relationships quickly, comfortable working with and through a range of different people
  * Empathetic and perceptive: sees the world from others' viewpoints, perceives both stated needs and deeper underlying needs, evaluates candidate potential and capabilities with ease, earns trust with candidates and internal stakeholders through responsiveness and understanding
  * Systematic and operational: translates strategy into a framework for how work should be executed, is able to establish processes to consistently handle and work in a planful way
  * Analytical Mindset: Strong data-driven decision making abilities, comfortable using tools and systems to execute one’s work, able to effectively communicate data outcomes to internal stakeholders
  * Resourceful: uses all the resources around them to seize opportunities and progress toward a goal; where resources are missing, improvises to find them or do without them
  * Resourceful: uses all the resources around them to seize opportunities and progress toward a goal; where resources are missing, improvises to find them or do without them
  * 2+ years of professional experience in human resources, Human capital or diversity consulting or talent acquisition.
  * Bachelor of Science or Bachelors of Arts degree required

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified candidates will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
* Next, candidates will be invited to schedule a 30 minute interview with our Recruiting Director.
* Next, candidates will be invited to schedule a 30 minute interview with at least one of our Recruiting team members.
* Then candidates may be invited to a 45 minute interview with one of our hiring managers.
* Candidates may also be asked to schedule a 45 minute interview with our Chief Culture Officer.
* Finally, our CEO may choose to conduct a final interview.

Additional details about our process can be found on our [hiring page](/handbook/hiring).
