---
layout: job_page
title: "Chief Culture Officer"
---

The GitLab team has grown quickly, and plans to continue grow while maintaining our culture and [remote working](/2015/04/08/the-remote-manifesto/), embracing development, and building scalable people-oriented policies. The VP of People Operations will be the a strategic leader and consultant that will oversee HR, Talent, and Learning & Development functions. In this role you'll be empowered to grow teams, making data driven decisions, and delivering transparent and people first programs.

## Responsibilities

- Provide strategy and oversight of the administration of compensation, benefits, HRIS     system
- Work to refine and improve our global compensation frameworks including the [Global Compensation Calculator](/handbook/people-operations/global-compensation/)
- Advise the executive team on relevant compliance and employment law and introduce best practices to protect the organization
- Develop and implement effective training programs for GitLab employees and community
- Harness the existing culture and values when building out internal people related policies while devising new ways to reinforce and improve upon the culture
- Manage talent initiatives from recruiting and onboarding to employee retention and offboarding
- Identify and measure organizational KPIs and OKRs for all of People Operations
- Implement global program for evaluating performance and providing feedback
- Advise company on all legal issues related to employment
- Evaluate and manage all third party People Ops vendors and partners
- Manage and grow a people operations team
- Report directly to the CEO


## Requirements

- 5+ years experience in a People Operations or HR executive position
- Recent experience in a People Ops function within a fast growing organization, preferably a tech/software company
- International HR or People Ops experience
- Enthusiasm for and broad experience with software tools
- Proven experience quickly learning new software tools
- Deep understanding and competence around US employment law and best practices, international employment law preferred
- Willingness to work with git and GitLab, using GitLab workflows within the People Ops team
- Commitment to making People Operations as open and transparent as possible
- Desire to work for a fast-moving startup and the ability to iterate quickly
- You share our [values](/handbook/values), and work in accordance with those values.
- Excellent written and verbal communication skills
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks)
