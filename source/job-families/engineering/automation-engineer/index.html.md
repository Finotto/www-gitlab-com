---
layout: job_page
title: "Automation Engineer"
---

GitLab is looking for a motivated and experienced engineer to help grow our test automation efforts across the entire GitLab ecosystem. This is a key position with a new and growing team, so your efforts will have a noticeable impact to both the company and product. In addition to the requirements below, successful candidates will demonstrate a passion for high quality software, strong engineering principles and methodical problem solving skills.

## Responsibilities

- Expand our existing test automation framework
- Develop new tests and tools for our GitLab.com frontend, backend APIs and services, and low-level systems like geo replication, CI/CD, and load balancing
- Work with the product team and other development teams to understand how new features should be tested, and then engage them in contributing automated tests
- Drive adoption of best practices in code health, testing, testability and maintainability. You should know about clean code, the test pyramid and champion these concepts.
- Analyze complex software systems and collaborate with others to improve the overall design, testability and quality.
- Ensure that automated tests execute reliably and efficiently in CI/CD environments.
- Ensure test results are tracked and communicated in a timely and effective manner

## Requirements

- Strong experience developing in Ruby
- Strong experience using Git
- Experience with test automation tools like Capybara, Selenium
- Relevant internship or work experience in software development and/or test automation
- Experience working with Docker containers
- Experience with AWS or Kubernetes
- Experience with Continuous Integration systems (e.g., Jenkins, Travis, GitLab)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team/).

- Qualified candidates receive a short questionnaire from our Recruiting team
- Selected candidates will be invited to schedule a 30 minute screening call with our Recruiting team
- Next, candidates will be invited to schedule a 45 minute first interview with the Director of Quality
- Candidates will then be invited to schedule a 1 hour technical interview with a Quality Engineer
- Candidates will be invited to schedule a third 45 minute interview with our VP of Engineering
- Finally, candidates will schedule a 50 minute interview with our CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).
