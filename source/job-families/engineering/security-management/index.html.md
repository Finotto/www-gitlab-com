---
layout: job_page
title: "Security Engineering Management"
---

## Security Engineering Management Roles at GitLab

Managers in the security engineering department at GitLab see the team as their product. While they are technically credible and know the details of what security engineers work on, their time is spent hiring a world-class team and putting them in the best position to succeed. They own the delivery of security commitments and are always looking to improve productivity. They must also coordinate across departments to accomplish collaborative goals.

### Security Engineering Manager

* Hire a world class team of security engineers to work on their team
* Help security engineers grow their skills and experience
* Conducts reviews on security architecture, issues, and features
* Hold regular 1:1's with all members their team
* Create a sense of psychological safety on your team
* Recommend security-related technical and process improvements
* Exquisite written and verbal communication skills
* Author project plans for security initiatives
* Draft quarterly OKRs
* Train engineers to screen candidates and conduct managerial interviews
* Strong sense of ownership, urgency, and drive
* Excellent written and verbal communication skills, especially experience with executive-level communications
* Ability to make concrete progress in the face of ambiguity and imperfect knowledge

#### To Review from Security Lead

* Find and fix security issues within the GitLab code base
* Define, implement, and monitor security measures to protect GitLab.com and company assets
* Manage a [bug bounty program](https://medium.com/@collingreene/bug-bounty-5-years-in-c95cda604365#.blaaokpi9)
* Perform vulnerability testing, risk analyses, and security assessments, and
follow through on implementation, working across teams.
* Investigate intrusion incidents, conduct forensic investigations, and mount incident responses
* Collaborate with colleagues on authentication, authorization and encryption solutions
* Evaluate new technologies and processes that enhance security capabilities
* Analyze and advise on new security technologies and program conformance
* Write documentation around how to maintain a high-level of security.

##### Specific security-related efforts or projects

- Secure Development lifecycle (SDL) process guidance
- Style guides and design best practices for engineering
- Courses for engineering (including guest speakers)
- Reduce surface area in application (Git Annex, old API)
- Post-postmortems on found security bugs (helps people think about it, high leverage)
- Document security trade-offs
- Automated testing/linting
- Compliance (HIPAA)
- Offensive (pen testing)
- Detection and response (monitoring, Detection, IDS, OSSEC, updates, response)
- Network security (teleport, VPC's, firewalls, access control, also IDS)
- Patch management / Vulnerability management and coordination (modeled after relevant ISO standard)
- Defense in depth recommendations
- Penetration testing by externals
- Source code analysis by externals
- Bug bounty program
- Endpoint security (fleetsmith, encryption, phishing reporting, yubikey, reducing access)
- Runbooks for incidents, recovery plans
- Abuse (spam, bitcoin mining)
- Package infrastructure/update/release process/patches
- Communication (blog post, postmortems, incident response/crisis communication)
- Dependencies and contribution security risks
- Credential management (Vault)

## Requirements (from Security Lead)

- Experience with application and SaaS security experience in production-level settings.
- This position requires some development experience and high level of
familiarity with common security libraries, security controls, and common
security flaws that apply to Ruby on Rails applications.
- Passion for open source
- Linux experience (e.g. Ubuntu)
- Programming experience (Ruby and Ruby on Rails preferred; for GitLab debugging)
- Collaborative team spirit with great communication skills
- You share our [values](/handbook/values), and work in accordance with those values.

### Director of Security

The Director of Security role extends the [Security Engineering Manager](#security-engineering-manager) role.

* Own all of GitLab security, including security compliance, internal and product security
* Hire a world class team of managers and security engineers to work on their teams
* Help their managers and security engineers grow their skills and experience
* Manage multiple teams and projects
* Hold regular skip-level 1:1's with all members of their team
* Create a sense of psychological safety on your teams
* Drive technical and process improvements
* Drive quarterly OKRs
* Represent the company publicly at conferences

#### Responsibilities (To review from Director of Security)

- Secure our products (GitLab CE/EE), services (GitLab.com, package servers, other infrastructure), and company (laptops, email).
- Keep our risk analysis up to date.
- Define and plan priorities for security related activities based on that risk analysis.
- Determine appropriate combination of internal security efforts and external
security efforts including bug bounty programs, external security audits
(penetration testing, black box, white box testing).
- Analyze and advise on new security technologies.
- Build and manage a team, which currently consists of our [Security Lead](/job-families/engineering/security-management)
and [Security Engineers](/job-families/engineering/security-engineer).
   - Identify and fill positions.
   - Grow skills in team leads and individual contributors, for example by
   creating training and testing materials.
   - Deliver input on promotions, function changes, demotions, and terminations.
- Ensure our engineers and contributors from the wider community run a secure software development lifecycle for GitLab by training them in best practices and creating automated tools.
- Respond to security and service abuse incidents.
- Perform red team security testing of our product and infrastructure.
- Run our bounty program effectively.
- Ensure we're compliant with our legal and contractual security obligations.

##### Requirements (To review from Director of Security)

- Significant application and SaaS security experience in production-level settings.
- This position does not require extensive development experience but the
candidate should be very familiar with common security libraries, security
controls, and common security flaws that apply to Ruby on Rails applications.
- Experience managing teams of engineers, and leading managers.
- Experience with (managing) incident response.
- You share our [values](/handbook/values), and work in accordance with those values.

###### Hiring Process (To review from Director of Security)

The hiring process for this role consists of _at least_

- [screening call](/handbook/hiring/#screening-call)
- interview with Security Lead
- interview with Director of Infrastructure
- interview with VP of Engineering
- interview with CEO

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).
