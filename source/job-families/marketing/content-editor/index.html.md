---
layout: job_page
title: "Content Editor"
---

## Responsibilities

- Contribute to the design, planning and execution of content marketing campaigns and strategies.
- Develop a consistent GitLab voice guide for other team members to refer to.
- Own the GitLab blog:
    - Actively commission and edit posts by team members and external writers
    -  Assist in developing posts requested within Marketing and by other departments or partners
    - Be the gatekeeper of quality on the blog
    - Work with the [Associate Social Marketing Manager](/job-families/marketing/social-marketing-manager/) to ensure promotion of new posts
- Sub-edit the contributions of other marketing team members to create a consistent GitLab brand and voice.
- Maintain a high standard of well written and factually accurate content.
- Develop a deep understanding of GitLab's current and upcoming products and features.

## Requirements

- Degree in English, journalism or media.
- 3-5 years experience as a writer and editor in enterprise marketing.
- A dual-minded approach: Highly creative and an excellent writer/editor but can also be process-driven, think scale, and rely on data to make decisions.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external contributors.
- Extremely detail-oriented and organized, able to meet deadlines.
- Deep understanding of the software development process including Git, CI and CD
- Obsessive about content quality not quantity.
- Regular reporting on how content and channel performance to help optimize our content marketing efforts.
- You share our [values](/handbook/values), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission.
