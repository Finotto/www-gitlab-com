---
layout: job_page
title: "Chief Marketing Officer (CMO)"
---

## About the Position

The CMO will report to the CEO and will play a vital leadership role in ensuring rapid and sustainable growth in demand to further drive sales growth. The CMO will oversee our overall marketing strategy across all marketing initiatives, acquisition channels, and distribution channels. The CMO will manage a growing global team of directors for sales development, demand generation including field marketing, product marketing, outreach to developers and contributions, corporate marketing and public relations. The goal is to grow awareness for GitLab as the single application for the entire DevOps lifecycle and to generate the demand we need to keep doubling our incremental annual contract value year over year. Qualified candidates will be experienced technology marketing leaders in subscription-based enterprise software, demand generation, and online marketing, with a track record of executing successful marketing strategies.

## Teams

The Marketing department consists of the following teams:

* Community Marketing
  * Community Advocacy
  * Community Programs
* Corporate Marketing
  * Content
  * Corporate Events
  * Design
  * Public Relations
* Pipe-to-Spend
  * Field Marketing
  * Marketing Operations
  * Marketing Program Management
  * Online Growth  
* Product Marketing
  * Analyst Relations
  * Sales Enablement
  * Product Marketing
  * Partner Marketing
* Sales Development
  * Inbound
  * Outbound

See the [marketing handbook](/handbook/marketing/) for details.

## Responsibilities

* Care deeply about GitLab’s teammates, contributors, and users with the ability to translate your leadership into increased revenue, brand halo, and developer relations.
* Build lasting relationships with your teammates and partners as the executive overseeing the leadership for all marketing deliverables. 
* Proactively identify and develop longer-term marketing and communications strategies and initiatives that will result in increased visibility and profitable growth.
* Build, execute and maintain a scalable demand generation model to drive sales growth.
* Articulate compelling value propositions and drive adoption for GitLab offerings in various market segments and geographical areas.
* Drive strategy and execution for demand generation and brand awareness campaigns across all digital and offline channels.
* Design and implement high-impact programs that are optimized to deliver results across the marketing and sales funnel, while hitting target metrics, Objectives and Key Results (OKR’s) and marketing ROI.
* Develop, mentor, and grow our in-house Marketing teams globally, keeping GitLab’s values at the forefront for the team.
* Manage our marketing budget and forecast.
* Manage content development and position GitLab as the single application for entire DevOps lifecycle through thought leadership, white papers, infographics, blog posts, social media, webinars and by being an evangelist for our software and company.
* Contribute to the creation of a comprehensive global marketing plan, digital marketing (web, SEO/SEM), social media-focused marketing, analyst relations, demand generation, events, and branding.


## Preferred Requirements

* 15+ years of enterprise software experience.
* Knowledge on how to guide a brand's reputation and expand demand generation.
* Experience in building, executing and measuring data-driven marketing programs across online and traditional channels.
* Experience in the use of analytics tools and ROI metrics.
* Deep experience in leadership within a global technology sector with a proven track record in growing market share.
* BA or BS degree in Marketing, Business or related field.
* Excellent strategic and analytical skills combined with the creativity to produce innovative breakthrough value propositions and businesses for the company.