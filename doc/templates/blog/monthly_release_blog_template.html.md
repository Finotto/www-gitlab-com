---
release_number: "X.Y" # version number - required
title: "GitLab X.Y released with Feature A and Feature B" # short title - required
author: Joshua Lambert # author name and surname - required
author_gitlab: joshlambert # author's gitlab.com username - required
author_twitter: # author's twitter username - optional
image_title: '/images/X_X/X_X-cover-image.jpg' # cover image - required
description: "GitLab X.Y released with Feature A, Feature B, Feature C, Feature D and much more!" # short description - required
twitter_image: '/images/tweets/gitlab-X-Y-released.png' # social sharing image - not required but recommended
categories: releases # required
layout: release # required
# header_layout_dark: true #uncomment if the cover image is dark
# release_number_dark: true #uncomment if you want a dark release number
---

<!--
This is the release blog post file. Add here the introduction only.
All remaining content goes into data/release-posts/.

**Use the merge request template "Release-Post", and please set the calendar due
date for each stage (general contributions, review).**

Read through the Release Posts Handbook for more information:
https://about.gitlab.com/handbook/marketing/blog/release-posts/
-->

Introductory paragraph.

<!--
Suggestion (to help you to get started): in one paragraph, summarize what are 
he most important _achievements_ of this release. Focus on the most impactful
things. E.g., improvements to performance, collaboration, quality assurance,
high availability, security, etc.
-->

<!-- more -->

Introduction.

[Markdown](/handbook/product/technical-writing/markdown-guide/) supported.

<!--
Suggestion: describe each feature briefly in just a few words, using
anchors to link to their headers. The intro is supposed to be eyes-catching,
so "be happy" about it, describe them enthusiastically. Focus on what are the
advantages on having each of them. For some guidance, look at the intros of
past release posts.
-->
